#ifndef GRAPHICS_H
#define GRAPHICS_H

//GRAPHICS CLASS
//Holds all info for dealing with game's graphics

#include <map>
#include <string>

// can forward declare these structs and pointers even tho they haven't been included yet with SDL library
struct SDL_Window;
struct SDL_Renderer;

class Graphics{
public:
	Graphics();
	~Graphics();

	/* SDL_Surface *loadImage
		Loads image into the _spriteSheets map if not already exists,
		so each image loaded only once.
		Returns the image from the std::map whether or not it was just loaded
	*/
	SDL_Surface *loadImage(const std::string &filepath);

	/*void blitSurface
	draws images (SDL_Textures)
	draw a texture to a certain part of the screen aka where `destinationRectangle` is*/
	void blitSurface(SDL_Texture *texture, SDL_Rect *sourceRectangle, SDL_Rect *destinationRectangle);

	void flip();						// Renders everything ot screen
	void clear();						// Clears screen

	SDL_Renderer *getRenderer() const;	// Returns the renderer
private:
	SDL_Window		*_window;
	SDL_Renderer	*_renderer;
	std::map<std::string, SDL_Surface*> _spriteSheets;	// holds in-memory spritesheets in our game -- with this, no need to load an image more than once
};


#endif
