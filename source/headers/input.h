#ifndef INPUT_H
#define INPUT_H

#include <SDL2/SDL.h>
#include <map>

class Input {
public:
	void beginNewFrame();
	void keyDownEvent(const SDL_Event &event);
	void keyUpEvent(const SDL_Event &event);

	bool wasKeyPressed(SDL_Scancode key);	// pass in any key to this function, it'll tell us if key was pressed
	bool wasKeyReleased(SDL_Scancode key);
	bool isKeyHeld(SDL_Scancode key);
private:
	std::map<SDL_Scancode,bool> _heldKeys;	// keys currently held ;; SDL_Scancode represents keyboard input?
	std::map<SDL_Scancode,bool> _pressedKeys;
	std::map<SDL_Scancode,bool>  _releasedKeys;
};

#endif
