#ifndef SPRITE_H
#define SPRITE_H

#include <SDL2/SDL.h>
#include <string>

class Graphics;

/*
Sprite class
BG sprites, enemies, etc derived from this
*/

class Sprite{
public:
	Sprite();

	/*
	filepath is where spritesheet image is
	sourceX, sourceY = where on spritesheet to get the sprite
	height and width are dimensions for sprite on spritesheet
	posX and posY are where Sprite will start out on screen
	*/
	Sprite(Graphics &graphics, const std::string &filepath, int sourceX, int sourceY, int width, int height, float posX, float posY);

	virtual ~Sprite();
	virtual void update();	// virtual cause we'll override this for animated sprites and other kinds of sprites
	void draw(Graphics &graphics, int x, int y);

private:
	SDL_Rect 	_sourceRect;	// where on spritesheet we'll get sprite
	SDL_Texture	*_spriteSheet;	// pointer to spritesheet responsible for this sprite

	float _x, _y;
};

#endif
