//Graphics

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "graphics.h"
#include "globals.h"

//GRAPHICS CLASS
//Holds all info for dealing with game's graphics
const int flags=			0;
const char windowTitle[]=	"Cave Story remake";

Graphics::Graphics(){
	SDL_CreateWindowAndRenderer(globals::SCREEN_WIDTH, globals::SCREEN_HEIGHT, flags, &this->_window, &this->_renderer);
	SDL_SetWindowTitle(this->_window, windowTitle);
}

Graphics::~Graphics(){
	SDL_DestroyWindow(this->_window);
}

SDL_Surface *Graphics::loadImage(const std::string &filepath){
	if( this->_spriteSheets.count(filepath) == 0 ){ // spritesheet not yet loaded, system can't find it
		this->_spriteSheets[filepath]=	IMG_Load(filepath.c_str());
	}

	return this->_spriteSheets[filepath];
}

void Graphics::blitSurface(SDL_Texture *texture, SDL_Rect *sourceRectangle, SDL_Rect *destinationRectangle){
	SDL_RenderCopy(this->_renderer, texture, sourceRectangle, destinationRectangle);
}

void Graphics::flip(){
	SDL_RenderPresent(this->_renderer);
}

void Graphics::clear(){
	SDL_RenderClear(this->_renderer);
}

SDL_Renderer *Graphics::getRenderer() const {
	return this->_renderer;
}
