#include <SDL2/SDL.h>

#include "game.h"
#include "graphics.h"
#include "input.h"


//==== GAME CLASS ====
namespace {
	const int FPS=				50;									// Our frames-per-second goal
	const int ONE_SECOND_MS=	1000;								// one second in milliseconds aka 5,000 milliseconds
	const int MS_MULTIPLE=		5;
	const int MAX_FRAME_TIME=	MS_MULTIPLE * ONE_SECOND_MS / FPS;	// max time a frame is allowed to last
}

//Holds all info for our game loop
Game::Game(){
	// sets up stuff like timer,audio, joystick, events, etc...
	SDL_Init(SDL_INIT_EVERYTHING);
	this->gameloop();
}

Game::~Game(){

}

void Game::gameloop(){
	Graphics graphics;
	Input input;
	SDL_Event event; // holds whatever event happens in frame

	this->_player=	Sprite(graphics,"content/sprites/MyChar.png",0,0,16,16,100,100);

	int LAST_UPDATE_TIME=	SDL_GetTicks();	// Gets number of milliseconds since SDL lib was init'd

	// in each while loop, SDL checks for a bunch of different events...
	while(true){
		input.beginNewFrame();	// clear keys from previous frame that are no longer relevant

		if(SDL_PollEvent(&event)){ // scans for events
			if(event.type == SDL_KEYDOWN){
				if(event.key.repeat==0){ // if this is not a key repeat ... double-tap?
					input.keyDownEvent(event);
				}
			}
			else if(event.type == SDL_KEYUP){
				input.keyUpEvent(event);
			}
			else if(event.type == SDL_QUIT){ // if SDL find an event, and event type is user clicking X on window, end the loop
				return;
			}
		}

		if(input.wasKeyPressed(SDL_SCANCODE_ESCAPE) == true){
			return;
		}

		const int CURRENT_TIME_MS=	SDL_GetTicks();						// how long since SDL init
		int ELAPSED_TIME_MS=		CURRENT_TIME_MS - LAST_UPDATE_TIME;	// how long this loop took, in milliseconds

		// If the loop took shorter than how long a frame should last,
		// use loop time, else use max frame time -- this limits to 50 fps
		// Physics of the game based on the numbers below
		this->update(std::min(ELAPSED_TIME_MS, MAX_FRAME_TIME));
		LAST_UPDATE_TIME=	CURRENT_TIME_MS;

		this->draw(graphics);
	}
}

void Game::draw(Graphics &graphics){
	graphics.clear();

	this->_player.draw(graphics,100,100);
	graphics.flip();
}

void Game::update(float elapsedTime){

}
