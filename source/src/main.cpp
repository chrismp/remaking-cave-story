//main.cpp
//entry point of program

#include "game.h"

int main(){
	Game game; // calls constructor for Game class, which inits SDL stuff and runs gameLoop()

	return 0;
}
