#include "input.h"

// Input class
// keeps track of keyboard state

// called at beginning of each new frame to reset keys that are no longer relevant
void Input::beginNewFrame(){
	this->_pressedKeys.clear();
	this->_releasedKeys.clear();
}

// called when key is being pressed
void Input::keyDownEvent(const SDL_Event &event){
	this->_pressedKeys[event.key.keysym.scancode]=	true;	// press a key, this std::map is set to true since it expects a `bool`
	this->_heldKeys[event.key.keysym.scancode]=		true;	// since we're pressing a key, it's held too
}

// called when a key is reeased
void Input::keyUpEvent(const SDL_Event &event){
	this->_releasedKeys[event.key.keysym.scancode]=	true;	// when a key is released, that's a key-up event,so we set this to true
	this->_heldKeys[event.key.keysym.scancode]=		false;	// already was set true by keyDownEvent(), and this is opposite, so it's false
}

// check if certain key was pressed during current frame
bool Input::wasKeyPressed(SDL_Scancode key){
	return this->_pressedKeys[key];
}

// check if certain key was released
bool Input::wasKeyReleased(SDL_Scancode key){
	return this->_releasedKeys[key];
}

// checks if certain key is currently being held
bool Input::isKeyHeld(SDL_Scancode key){
	return this->_heldKeys[key];
}
