learning from https://youtu.be/ETvApbD5xRo?list=PLNOBk_id22bw6LXhrGfhVwqQIa-M2MsLa

/source/headers/ -- header files
/source/src/ -- implmentation files


# Lesson 1
- We need a window on which to draw our game

- all grafx in one class
- resolutiongonna be 640x480

- Split up headers/implementation files
- make gfx class 
- call it from main() ((for now))


# Lesson 2
## Problem
- Need more central location for game loop logic
- events (input, closing window, etc)
- limit FPS (Cave Story runs at 50FPS)
- Rest of Game class
## Solution
- Make and implement classes for game and input
- Add input to game loop (so we can actually close our window and stuff)
- Limit FPS


# LESSON 3
## PROBLEM
- Probably better ot keep comments in just header files
- Need to draw thigns to screen -- specifically our character, Quote
- Graphics class don't do much right now
- Can't load PNG files yet
##Details
- Spritesheet - MyChar.png
- Sprites are scaled by 2x?